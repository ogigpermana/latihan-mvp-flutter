import 'package:flutter/material.dart';
import 'module/contacts/contact_view.dart';
import 'injection/dependency_injection.dart';

void main() {
  Injector.configure(Flavor.PRO);

  runApp(
    new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Latihan MVP Pattern',
      theme: new ThemeData(
        primarySwatch: Colors.blue
      ),
      home: new ContactsPage()
    )
  );
}