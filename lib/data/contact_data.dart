import 'dart:async';
import 'package:intl/intl.dart';
// Data gathering
// Let´s start creating the data sources clients. 
// Create a new folder called data inside lib directory. 
// Next step is creating contact_data.dart file inside data folder.

// In this file the Contact class and the interface that will expose 
// how to get contacts and also an exception that will notice 
// in case anything went wrong during the data fetch will be defined.

// So, the contacts list should be place in another file as a mock implementation.

// Check the result:

class Contact {

  static final DateFormat _formatter = DateFormat('MMMM d, yyyy');

  final String fullName;
  final String gender;
  final String email;
  final String imageUrl;
  final String birthday;
  final String national;
  final Location location;
  final List<Phone> phones;

  const Contact({this.fullName, this.gender, this.email, this.imageUrl,
     this.birthday, this.national, this.location, this.phones});

// Inside results, you can iterate every element using a Map 
// and turn them into Contact and return the list using toList method.

// To do so, a new Contact constructor that allows 
// to create contacts from a Json must be defined.

  Contact.fromMap(Map<String, dynamic>  map) :
                    fullName = "${map['name']['first']} ${map['name']['last']}",
                    gender = map['gender'],
                    email = map['email'],
                    imageUrl = map['picture']['large'],
                    birthday = _formatter.format(DateTime.parse(map['dob']['date'])),
                    national = map['nat'],
                    location = Location.fromMap(map['location']),
                    phones = <Phone>[
                      new Phone(type: 'Home',   number: map['phone']),
                      new Phone(type: 'Mobile', number: map['cell'])];

// In this constructor fullName and email attributes 
// are extracted from map and assign by name.
}

class Location {
  final String street;
  final String city;

  const Location({this.street, this.city});

  Location.fromMap(Map<String, dynamic>  map) :
                    street = map['street'],
                    city = map['city'];
}

class Phone {
  final String type;
  final String number;

  const Phone({this.type, this.number});
}

abstract class ContactRepository {
  Future<List<Contact>> fetch();
}

class FetchDataException implements Exception {
  String _message;

  FetchDataException(this._message);

  String toString() {
    return "Exception: $_message";
  }
}

// As it was said, this file contains the Contact class, 
// ContactRepository interface defining 
// a fetch method which returns a Future<ContactList> and FetchDataException.

// In order to use Future dart:async must be imported, 
// Future allows working using promises in Dart



