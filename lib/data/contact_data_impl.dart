import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'contact_data.dart';

// Random User Repository
// Its second implementation will be using RandomUser service, 
// // this will cover how to fetch data calling a service using Dart.
// In this case, the class implementing ContactRepository interface is RandomUserRepository.

// Inside the fetch method, execute a get function to query an URL contained inside _kRandomUserUrl. 
// In order to do this, first import package:flutter/http.dart. Using as operator makes the http variable containing the whole package.

// In order to perform the query, use http´s get method. 
// It returns a Future which is Dart´s way of using promises. 
// Future´s then method should be called when the query is finished 
// and all the information was already gathered.

// The then method takes a lambda as a parameter in order to notify the reply. 
// It returns body and a statusCode to check if everything is OK, 
// otherwise FetchDataException will be thrown.

// Last step is reading a Json, to do so, use dart:convert and JsonDecoder class 
// which takes reply´s body as a parameter.

class RandomUserRepository implements ContactRepository {

  static const _kRandomUserUrl = 'http://api.randomuser.me/?results=15';
  final JsonDecoder _decoder = new JsonDecoder();

  Future<List<Contact>> fetch() async {
    final response = await http.get(_kRandomUserUrl);
    final jsonBody = response.body;
    final statusCode = response.statusCode;

    if(statusCode < 200 || statusCode >= 300 || jsonBody == null) {
      throw new FetchDataException("Error while getting contacts [StatusCode:$statusCode, Error:${response.reasonPhrase}]");
    }

    final contactsContainer = _decoder.convert(jsonBody);
    final List contactItems = contactsContainer['results'];

    return contactItems.map( (contactRaw) => Contact.fromMap(contactRaw) )
                         .toList();   
  }

}

