// Import both dart:async and contact_data.dart in order to use Future. 
// ContactRepository and Contact.

import 'dart:async';
import 'contact_data.dart';

// Mock Repository
// Our first ContactRepository interface implementation will be a mocked one. 
// In this file a class that implements ContactRepository interface and 
// that returns the contacts list from the last post through the fetch method will be created.

class MockContactRepository implements ContactRepository{
  // As seen, a class to implement ContactRepository is created 
  // and inside the fetch method a contact list is returned using a Future.

  Future<List<Contact>> fetch(){
    return Future.value(kContacts);
  }

}

const kContacts = const <Contact>[
    const Contact(
      fullName: 'John Doe',
      email:'johndoe@example.com'
    ),
    const Contact(
      fullName: 'Jane Doe',
      email:'janedoe@example.com'
    )
];


