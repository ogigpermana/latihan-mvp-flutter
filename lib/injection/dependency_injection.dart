import '../data/contact_data.dart';
import '../data/contact_data_impl.dart';
import '../data/contact_data_mock.dart';


enum Flavor {
  MOCK,
  PRO
}

/// Simple DI
class Injector {
  static final Injector _singleton = Injector._internal();
  static Flavor _flavor;

  static void configure(Flavor flavor) {
    _flavor = flavor;
  }

  factory Injector() {
    return _singleton;
  }

  Injector._internal();

  ContactRepository get contactRepository {
    switch(_flavor) {
      case Flavor.MOCK: return MockContactRepository();
      default: // Flavor.PRO:
       return RandomUserRepository();
    }
  }
}

// Note :
// Flavor enumerator is used to identify both environments mock and production. 
// Injector class is a singleton, 
// it has a private constructor called _internal, a configure method to choose an environment, 
// a factory to return the singleton once the Injector is created 
// and a get method to return the ContactRepository implementation previously selected.

// Inside main.dart file, before runApp instruction, 
// execute configure method indicating which Flavor will be used. In this case PRO.