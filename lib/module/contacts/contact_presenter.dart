// First of all, define a Contract interface, 
// this will help to communicate the presenter and the UI. 
// Two methods are defined, one to notify when the contacts are already loaded 
// and another to notify if any error happened during the download.

import '../../data/contact_data.dart';
import '../../injection/dependency_injection.dart';


abstract class ContactListViewContract {
  void onLoadContactsComplete(List<Contact> items);
  void onLoadContactsError();
}

// Inside the constructor, the view contract is assigned 
// from and the Injector will select which ContactRepository 
// to be used between MockContactRepository or RandomUserRepository.

class ContactListPresenter {
  ContactListViewContract _view;
  ContactRepository _repository;

  ContactListPresenter(this._view){
    _repository = new Injector().contactRepository;
  }

// Create a method called loadContacts, 
// this is how the view will ask for contacts. 
// Inside this method, get a List<Contact> Future using fetch method 
// and use a lambda to be executed when the repository finishes 
// fetching the data and mapping it into a contacts list.

  void loadContacts(){
    assert(_view != null);

    _repository.fetch()
              .then((contacts) => _view.onLoadContactsComplete(contacts))
              .catchError((onError) {
                print(onError);
                _view.onLoadContactsError();
              });
  }

// By the time the data is available, lambda function should call 
// onLoadContactsComplete method using the contacts list 
// returned by the repository in order to pass the data to the view.

// If any exception is raised, catchError method will be executed 
// and it should print the error and communicate it to the view.

}
