import 'package:flutter/material.dart';
import '../contacts/contact_detail_view.dart';
import '../../data/contact_data.dart';
import 'contact_presenter.dart';

class ContactsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contacts With MVP Pattern"),
      ),
      body: ContactList(),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the Drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Logo'),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Item 1'),
              onTap: () {
                // Update the state of the app
                // ...
              },
            ),
            ListTile(
              title: Text('Item 2'),
              onTap: () {
                // Update the state of the app
                // ...
              },
            ),
          ],
        ),
      ),
    );
  }
}

///
///   Contact List
///
//Now, ContactList is not a StatelessWidget anymore.
// It currently is a StatefulWidget, also a State for the ContactList was created.

class ContactList extends StatefulWidget {
  ContactList({Key key}) : super(key: key);

  @override
  _ContactListState createState() => _ContactListState();
}

class _ContactListState extends State<ContactList>
    implements ContactListViewContract {
  ContactListPresenter _presenter;

  List<Contact> _contacts;

// The presenter is created inside the constructor and initState method
// the presenter is asked to gather the data.

  bool _isSearching;

  _ContactListState() {
    _presenter = ContactListPresenter(this);
  }

  @override
  void initState() {
    super.initState();
    _isSearching = true;
    _presenter.loadContacts();
  }

// ContactListState implements the Contract defined and both of its methods,
// onLoadContactsComplete and onLoadContactsError.

  @override
  void onLoadContactsComplete(List<Contact> items) {
    setState(() {
      _contacts = items;
      _isSearching = false;
    });
  }

  @override
  void onLoadContactsError() {
    // DO: implement onLoadContactsError
  }

  // When the presenter has the data, onLoadContactsComplete method is called,
  // use a lambda to call setState and invalidate the widget to force
  // the build method to be called again. This is how _isSearching is set to false
  // and by the time the build method is called again
  // it will return a MaterialList instead of a CircularProgressIndicator.

  @override
  Widget build(BuildContext context) {
// Inside the build method a CircularProgressIndicator is created
// to be shown to the user during the data fetch and then the list is generated.
// This progress is controlled using _isSearching variable,
// if true a CircularProgressIndicator is returned, if not a MaterialList.

    var widget;

    if (_isSearching) {
      widget = Center(
          child: Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0),
              child: CircularProgressIndicator()));
    } else {
      widget = ListView(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          children: _buildContactList());
    }

    return widget;
  }

  List<_ContactListItem> _buildContactList() {
    return _contacts
        .map((contact) => new _ContactListItem(
            contact: contact,
            onTap: () {
              _showContactPage(context, contact);
            }))
        .toList();
  }

  void _showContactPage(BuildContext context, Contact contact) {
    Navigator.push(
        context,
        new MaterialPageRoute<Null>(
            settings: const RouteSettings(name: ContactPage.routeName),
            builder: (BuildContext context) => new ContactPage(contact)));
  }
}

///
///   Contact List Item
///

class _ContactListItem extends ListTile {
  _ContactListItem(
      {@required Contact contact, @required GestureTapCallback onTap})
      : super(
            title: Text(contact.fullName),
            subtitle: Text(contact.email),
            leading: CircleAvatar(child: Text(contact.fullName[0])),
            onTap: onTap);
}
