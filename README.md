# latihan_mvp_architecture

This practice about how to implement MVP Pattern and produce the list of contacts
here is the result below:

![alt text](assets/flutter_01.png "Screenshot Application 1")
![alt text](assets/flutter_02.png "Screenshot Application 2")
![alt text](assets/flutter_03.png "Screenshot Application 3")

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
